import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.bytebuddy.build.Plugin;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Solution {
    public static void main(String[] args) {
        var jsonFactory = new JsonFactory();
        var objectMapper = new ObjectMapper(jsonFactory);
        var classLoader = Thread.currentThread().getContextClassLoader();
        try(var inputStream = classLoader.getResourceAsStream("tickets.json")) {
            var rootNode = objectMapper.readTree(inputStream);
            var tickets = rootNode.get("tickets");
            var ticketsFromVVOtoTLV = new ArrayList<JsonNode>();
            for (var ticket: tickets) {
                var ticketOriginAbbreviation = ticket.get("origin").textValue();
                var ticketDestinationAbbreviation = ticket.get("destination").textValue();
                if (ticketOriginAbbreviation.equals("VVO") && ticketDestinationAbbreviation.equals("TLV")) {
                    ticketsFromVVOtoTLV.add(ticket);
                }
            }

            var minimalFlightDurationFromVVOtoTLVPerCarrier = new HashMap<String, Long>();
            for (var ticket: ticketsFromVVOtoTLV) {
                var ticketDateFormat = new SimpleDateFormat("dd.MM.yy HH:mm");
                var ticketDepartureDateTime = ticketDateFormat
                        .parse(ticket.get("departure_date").textValue() +
                                " " +
                                ticket.get("departure_time").textValue())
                        .toInstant()
                        .atOffset(ZoneOffset.ofHours(3))
                        .toLocalDateTime();
                var ticketArrivalDateTime = ticketDateFormat
                        .parse(ticket.get("arrival_date").textValue() +
                                " " +
                                ticket.get("arrival_time").textValue())
                        .toInstant()
                        .atOffset(ZoneOffset.ofHours(3))
                        .toLocalDateTime();
                var flightDurationInMinutes = Duration
                        .between(ticketDepartureDateTime, ticketArrivalDateTime)
                        .toMinutes();

                var carrierName = ticket.get("carrier").textValue();
                if (minimalFlightDurationFromVVOtoTLVPerCarrier.containsKey(carrierName)) {
                    var flightDurationFromVVOtoTLVForCarrierFromTicket = minimalFlightDurationFromVVOtoTLVPerCarrier.get(carrierName);

                    if (flightDurationFromVVOtoTLVForCarrierFromTicket < flightDurationInMinutes) {
                        continue;
                    }
                }
                minimalFlightDurationFromVVOtoTLVPerCarrier.put(carrierName, flightDurationInMinutes);
            }

            for (var minimalFlightDurationFromTVOtoTLV:
                    minimalFlightDurationFromVVOtoTLVPerCarrier.entrySet()) {
                var minimalFlightDurationFromVVOtoTLVInMinutes = minimalFlightDurationFromTVOtoTLV
                        .getValue();
                System.out.println("Carrier: " + minimalFlightDurationFromTVOtoTLV.getKey());
                System.out.println("Flight duration: " +
                        minimalFlightDurationFromVVOtoTLVInMinutes / 60 +
                        " hours and " +
                        minimalFlightDurationFromVVOtoTLVInMinutes % 60 +
                        " minutes\n");
            }

            var averageTicketPriceFromVVOToTLV = (int)ticketsFromVVOtoTLV
                .stream()
                .mapToInt(t -> Integer.parseInt(t.get("price").asText()))
                .average()
                .orElse(0);

            ticketsFromVVOtoTLV
                .sort((t1, t2) -> {
                    var firstTicketPrice = Integer.parseInt(t1.get("price").asText());
                    var secondTicketPrice = Integer.parseInt(t2.get("price").asText());

                    return Integer.compare(firstTicketPrice, secondTicketPrice);
                });
            var medianTicketPriceFromVVOToTLV = Integer
                .parseInt(ticketsFromVVOtoTLV
                    .get(Math.floorDiv(ticketsFromVVOtoTLV.size(), 2))
                    .get("price")
                    .asText());
            if (ticketsFromVVOtoTLV.size() % 2 == 0) {
                medianTicketPriceFromVVOToTLV += Integer
                    .parseInt(ticketsFromVVOtoTLV
                        .get((ticketsFromVVOtoTLV.size() / 2) - 1)
                        .get("price")
                        .asText());
                medianTicketPriceFromVVOToTLV = Math.floorDiv(medianTicketPriceFromVVOToTLV, 2);
            }

            System.out.println("The difference between the average and median ticket price is " + (averageTicketPriceFromVVOToTLV - medianTicketPriceFromVVOToTLV) + " RUB");
        } catch(IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
